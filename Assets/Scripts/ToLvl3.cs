﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToLvl3 : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider player)
    {
        Time.timeScale = Time.timeScale * 1.2f;

        SceneManager.LoadScene("Lvl3");
    }
}
