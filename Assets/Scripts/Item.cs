﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    private GameController game;
    // Start is called before the first frame update
    private void Start()
    {
        game = FindObjectOfType<GameController>();
    }
    private void OnTriggerEnter(Collider player)
    {
        game.PickItem();
        Debug.Log("Item!");
        Destroy(gameObject);
    }
}
