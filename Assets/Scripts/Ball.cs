﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private float ForwardSpeed = 200;
    [SerializeField] private float RotationSpeed =100;

    private Rigidbody body;
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float forward = Input.GetAxis("Vertical") * ForwardSpeed * Time.deltaTime;
        float sidewards = Input.GetAxis("Horizontal") * RotationSpeed * Time.deltaTime;
        transform.Rotate(Vector3.up, sidewards);
        transform.position += transform.forward * Time.deltaTime * forward;
        //body.velocity = new Vector3(forward, 0, sidewards);
    }
}
