﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameController : MonoBehaviour
{
    [SerializeField] Text timer;
    [SerializeField] Text over;

    private Door door;
    private int itemsPicked = 0;
    static private double timeLeft = 60;
    // Start is called before the first frame update
    void Start()
    {
        door = FindObjectOfType<Door>();
        over.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        timeLeft = timeLeft - 1 * Time.deltaTime;
        timer.text = "Time left: " + (int)  timeLeft;
        //Debug.Log(timeLeft);
        if(timeLeft <= 0)
        {
            StartCoroutine(GameOver());
        }
    }

    public void PickItem()
    {
        itemsPicked++;
        if (itemsPicked >= 3)
            door.destroyExit();
    }

    IEnumerator GameOver()
    {
        over.enabled = true;
        Time.timeScale = 0.00001f;
        yield return new WaitForSecondsRealtime(2);
        SceneManager.LoadScene(0);
    }
}
