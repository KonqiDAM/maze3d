﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Transform[] wayPoints;
    [SerializeField] float speed = 5;
    [SerializeField] float switchDistance = 0.1f;
    sbyte dir = 1;
    byte numNextPost = 0;
    Vector3 nextPosition;
    private int lives;
    void Start()
    {
        nextPosition = wayPoints[0].position;
        lives = 5;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, nextPosition, speed * Time.deltaTime);
        if( Vector3.Distance(transform.position, nextPosition) < switchDistance)
        {
            numNextPost+= (byte)dir;

            if (numNextPost >= wayPoints.Length || numNextPost < 0)
            {
                dir = (sbyte)-dir;
                numNextPost += (byte)dir;
            }

            nextPosition = wayPoints[numNextPost].position;
        }

        
    }
    private void OnTriggerEnter(Collider player)
    {
        Debug.Log("Hit!");
        //player.SendMessage("Hit!!");
        //Application.Quit();
    }

    
}
